#!/bin/bash

# Update and upgrade all packages
apt-get update -y &&  apt-get upgrade -y

# Install Squid proxy
apt-get install squid -y

apt-get install htpasswd -y 
apt-get install apache2-util -y

# Install UFW firewall
apt-get install ufw -y

# Add SSH to allow lost of UFW firewall
ufw allow ssh
ufw allow 443
ufw allow 444
ufw default deny 

#htpasswd /etc/squid/passwd dargahuser

# Download a squid.conf file and replace it with squid.conf in /etc/squid/
#sudo wget https://raw.githubusercontent.com/abdelhady/squid-proxy/master/squid.conf -O /etc/squid/squid.conf
rm /etc/squid/squid.conf
cp ./squid.conf /etc/squid/squid.conf
cp ./password /etc/squid/passwd

# Restart Squid service
systemctl restart squid
ufw enable 
